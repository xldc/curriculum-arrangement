
项目介绍：
-----------------------------------
该项目为一套适用于学校，培训机构等进行一键排课的demo系统，可以根据自己的业务需要集成到自己的项目中，采用自己的表和字段对系统中涉及的字段进行替换  
为满足快速实现核心功能，项目采用Jeecg-boot框架进行开发，其中所有的基础管理功能（增删改查）直接使用了在线开发（online），并在菜单中配置了online菜单，这块的具体使用可以参见jeecg-boot文档。
另外就是教学计划中有一个list列表的js增强，实现点击自定义按钮【生成课表】调用后台action生成课表；还有一个form表单的js增强，用于表单页面计算共多少节课。

项目有两个版本：  
V1.0 - 对应v1.0分支，采用一般手段实现基础的排课功能，哪个老师在哪节课上哪个班级的哪一门课  
V2.0 - 对应master分支，采用jgap遗传算法实现，可以实现走班排课

使用说明
-----------------------------------
- 完整数据库表在`jeecg-boot/db/jf2curriculum4sancai.sql`中(不同分支该文件内容不一样)
- 后台访问账密  `admin / 123456`
- 其中课表管理为生成的课表信息，其他功能都是维护生成课表所需的相关数据
- 教学计划的作用就是设置哪个老师上哪个班级的哪一门课，每周上多少节
- 维护好基础数据后，在教学计划的列表页，点击【生成课表】按钮即可生成课表信息，在课表管理中进行查看

## 说明
因为该项目只为实现核心功能的demo，所以其中有些地方是不完美的，比如教学计划表单编辑页面中的班级、年级、学期选择可以做成联动；
再比如为了显示方便，数据库表中关联其他表的地方都将其id,code,name进行了存储


## 参考资料
http://galudisu.info/2018/07/24/algorithm/ga/genetic-algorithm/  ||  https://github.com/barudisshu/syllabus  
https://github.com/lzlz000/class-sort  
https://gitee.com/lequal/CourseArrange.git  
https://github.com/H-ing/CCS2.0  
https://blog.csdn.net/weixin_43999137/article/details/113178364  
https://blog.csdn.net/leangx86/article/details/103882399   
https://gitee.com/badger/jackfruit?_from=gitee_search  
https://sourceforge.net/projects/jgap/
