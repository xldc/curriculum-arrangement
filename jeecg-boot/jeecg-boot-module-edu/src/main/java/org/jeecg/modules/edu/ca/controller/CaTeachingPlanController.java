package org.jeecg.modules.edu.ca.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.ca.entity.CaTeachingPlan;
import org.jeecg.modules.edu.ca.service.ICaTeachingPlanService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Api(tags="教学计划")
@RestController
@RequestMapping("/ca/caTeachingPlan")
@Slf4j
public class CaTeachingPlanController extends JeecgController<CaTeachingPlan, ICaTeachingPlanService> {
	@Autowired
	private ICaTeachingPlanService caTeachingPlanService;
	
	/**
	 * 分页列表查询
	 *
	 * @param caTeachingPlan
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "教学计划-分页列表查询")
	@ApiOperation(value="教学计划-分页列表查询", notes="教学计划-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CaTeachingPlan caTeachingPlan,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CaTeachingPlan> queryWrapper = QueryGenerator.initQueryWrapper(caTeachingPlan, req.getParameterMap());
		Page<CaTeachingPlan> page = new Page<CaTeachingPlan>(pageNo, pageSize);
		IPage<CaTeachingPlan> pageList = caTeachingPlanService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param caTeachingPlan
	 * @return
	 */
	@AutoLog(value = "教学计划-添加")
	@ApiOperation(value="教学计划-添加", notes="教学计划-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CaTeachingPlan caTeachingPlan) {
		caTeachingPlanService.save(caTeachingPlan);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param caTeachingPlan
	 * @return
	 */
	@AutoLog(value = "教学计划-编辑")
	@ApiOperation(value="教学计划-编辑", notes="教学计划-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CaTeachingPlan caTeachingPlan) {
		caTeachingPlanService.updateById(caTeachingPlan);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "教学计划-通过id删除")
	@ApiOperation(value="教学计划-通过id删除", notes="教学计划-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		caTeachingPlanService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "教学计划-批量删除")
	@ApiOperation(value="教学计划-批量删除", notes="教学计划-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.caTeachingPlanService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "教学计划-通过id查询")
	@ApiOperation(value="教学计划-通过id查询", notes="教学计划-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CaTeachingPlan caTeachingPlan = caTeachingPlanService.getById(id);
		if(caTeachingPlan==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(caTeachingPlan);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param caTeachingPlan
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CaTeachingPlan caTeachingPlan) {
        return super.exportXls(request, caTeachingPlan, CaTeachingPlan.class, "教学计划");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CaTeachingPlan.class);
    }


	 /**
	  * 生成课表
	  *
	  * @param
	  * @return
	  */
	 @AutoLog(value = "教学计划-生成课表")
	 @ApiOperation(value="教学计划-生成课表", notes="教学计划-生成课表")
	 @PostMapping(value = "/createCurriculum")
	 public Result<?> createCurriculum() {
		 return caTeachingPlanService.createCurriculum();
	 }

}
