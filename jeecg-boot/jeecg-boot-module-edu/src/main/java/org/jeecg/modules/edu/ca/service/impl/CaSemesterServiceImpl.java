package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaSemester;
import org.jeecg.modules.edu.ca.mapper.CaSemesterMapper;
import org.jeecg.modules.edu.ca.service.ICaSemesterService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 学期
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaSemesterServiceImpl extends ServiceImpl<CaSemesterMapper, CaSemester> implements ICaSemesterService {

}
