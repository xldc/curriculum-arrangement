package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaProfessional;
import org.jeecg.modules.edu.ca.mapper.CaProfessionalMapper;
import org.jeecg.modules.edu.ca.service.ICaProfessionalService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专业
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaProfessionalServiceImpl extends ServiceImpl<CaProfessionalMapper, CaProfessional> implements ICaProfessionalService {

}
