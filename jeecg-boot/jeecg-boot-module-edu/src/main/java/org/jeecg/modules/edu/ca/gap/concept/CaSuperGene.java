package org.jeecg.modules.edu.ca.gap.concept;

import org.jeecg.modules.edu.ca.gap.util.Constant;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.InvalidConfigurationException;
import org.jgap.supergenes.AbstractSupergene;
import org.jgap.supergenes.Supergene;

import java.io.Serializable;

public class CaSuperGene extends AbstractSupergene implements Serializable {

    public CaSuperGene() throws InvalidConfigurationException {
        super();
    }

    public CaSuperGene(Configuration conf) throws InvalidConfigurationException {
        super(conf);
    }

    public CaSuperGene(Configuration conf, Gene[] genes)  throws InvalidConfigurationException{
        super(conf, genes);
    }

    @Override
    public boolean isValid(final Gene[] a_case, final Supergene a_forSupergene) {
        return true;
    }

    @Override
    public String getPersistentRepresentation() throws UnsupportedOperationException {
        StringBuilder sb = new StringBuilder();
        sb
                .append(this.geneAt(Constant.CLASSROOM).getPersistentRepresentation())
                .append(GENE_DELIMITER)
                .append(this.geneAt(Constant.TIME).getPersistentRepresentation());
        return sb.toString();
    }
}
