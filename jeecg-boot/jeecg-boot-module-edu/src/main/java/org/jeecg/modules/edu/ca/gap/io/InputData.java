package org.jeecg.modules.edu.ca.gap.io;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.ca.entity.*;
import org.jeecg.modules.edu.ca.gap.entity.TeachingPlan;
import org.jeecg.modules.edu.ca.gap.entity.Time;
import org.jeecg.modules.edu.ca.gap.util.Constant;
import org.jeecg.modules.edu.ca.mapper.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InputData {

    @Autowired
    private CaSemesterMapper caSemesterMapper;
    @Autowired
    private CaGradeMapper caGradeMapper;
    @Autowired
    private CaTeacherMapper caTeacherMapper;
    @Autowired
    private CaStudentMapper caStudentMapper;
    @Autowired
    private CaCourseMapper caCourseMapper;
    @Autowired
    private CaProfessionalMapper caProfessionalMapper;
    @Autowired
    private CaClasssMapper caClasssMapper;
    @Autowired
    private CaTeachingPlanMapper caTeachingPlanMapper;
    @Autowired
    private CaWorkConfigMapper caWorkConfigMapper;
    @Autowired
    private CaClassroomMapper caClassroomMapper;
    @Autowired
    private CaCurriculumMapper caCurriculumMapper;

    private static final int YES = 1;
    private static final int NO = 2;

    public void read(){

        CaSemester semester = caSemesterMapper.selectOne(new LambdaQueryWrapper<CaSemester>().eq(CaSemester::getCurrent,1));
        if(semester == null || StringUtils.isBlank(semester.getId())){
            throw new JeecgBootException("请先设置当前学期");
        }
        //----------------------------------------------------------------------------
        //-----------------------------get plan data-----------------------------
        List<CaTeachingPlan> planList = caTeachingPlanMapper
                .selectList(new LambdaQueryWrapper<CaTeachingPlan>().eq(CaTeachingPlan::getSemester,semester.getId()));
        if(CollectionUtils.isEmpty(planList)){
            throw new JeecgBootException("请先设置教学计划");
        }
        List<CaClasss> classsList = caClasssMapper.selectList(new LambdaQueryWrapper<CaClasss>());
        if(CollectionUtils.isEmpty(classsList)){
            throw new JeecgBootException("班级信息为空");
        }
        List<CaCourse> courseList = caCourseMapper.selectList(new LambdaQueryWrapper<>());
        if(CollectionUtils.isEmpty(courseList)){
            throw new JeecgBootException("课程信息为空");
        }
        /**
         * List -> Map
         * 需要注意的是：
         * toMap 如果集合对象有重复的key，会报错Duplicate key ....
         *  classs1,classs2的id都为1。
         *  可以用 (k1,k2)->k1 来设置，如果有重复的key,则保留key1,舍弃key2
         */
        Map<String,CaClasss> classsMap = classsList.stream().collect(Collectors.toMap(CaClasss::getId,c -> c,(k1,k2) -> k1));
        Map<String,CaCourse> courseMap = courseList.stream().collect(Collectors.toMap(CaCourse::getId,c -> c,(k1,k2) -> k1));
        Constant.PLAN_LIST = planList.stream().flatMap(p -> {
            List<TeachingPlan> list = new ArrayList<>();
            for (int i = 0; i < p.getWeeklyCourseNum(); i++) {
                TeachingPlan tp = new TeachingPlan();
                BeanUtils.copyProperties(p,tp);

                CaClasss classs = classsMap.get(p.getClasss());
                CaCourse course = courseMap.get(p.getCourse());
                if(classs == null){
                    throw new JeecgBootException(String.format("班级%s不存在",p.getClasssName()));
                }
                if(course == null){
                    throw new JeecgBootException(String.format("课程%s不存在",p.getCourseName()));
                }
                tp.setClasssSize(classs.getExistingNum());
                tp.setClassroomType(course.getClassroomType());
                tp.setFirstTime(course.getFirstTime());
                tp.setSecondTime(course.getSecondTime());
                tp.setThirdTime(course.getThirdTime());
                tp.setFourthTime(course.getFourthTime());
                list.add(tp);
            }
            return list.stream();
        }).collect(Collectors.toList());

        //----------------------------------------------------------------------------
        //----------------------------get classroom data-----------------------------
        List<CaClassroom> classroomList = caClassroomMapper.selectList(new LambdaQueryWrapper<>());
        if(CollectionUtils.isEmpty(classroomList)){
            throw new JeecgBootException("请先添加教室");
        }
        Constant.CLASSROOM_LIST = classroomList;

        //----------------------------------------------------------------------------
        //----------------------------get time data-----------------------------
        List<CaWorkConfig> workList = caWorkConfigMapper.selectList(new LambdaQueryWrapper<CaWorkConfig>());
        if(CollectionUtils.isEmpty(workList)){
            throw new JeecgBootException("作息计划未配置");
        }
        Constant.TIME_LIST = workList.stream().flatMap(c -> {
            List<Time> timeList = new ArrayList<>();
            int courseOrder = c.getCourseOrder();
            Integer[] arr = new Integer[]{ c.getMonday(),c.getTuesday(),c.getWednesday(),c.getThursday(),c.getFriday(),c.getSaturday(),c.getWeekday()};
            for (int i = 0;i<arr.length;i++){
                if(arr[i] == YES){
                    timeList.add(new Time().setWeekOrder(i+1).setCourseOrder(courseOrder));
                }
            }
            return timeList.stream();
        }).collect(Collectors.toList());
        //每天最多几节课
        Constant.COURSE_NUM_BY_DAY = Constant.TIME_LIST.stream().map(c -> c.getCourseOrder()).max(Integer::compareTo).get();

        //----------------------------------------------------------------------------


    }
}
