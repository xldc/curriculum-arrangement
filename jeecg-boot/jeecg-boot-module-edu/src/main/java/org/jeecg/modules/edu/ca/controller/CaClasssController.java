package org.jeecg.modules.edu.ca.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.ca.entity.CaClasss;
import org.jeecg.modules.edu.ca.service.ICaClasssService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 班级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Api(tags="班级")
@RestController
@RequestMapping("/ca/caClasss")
@Slf4j
public class CaClasssController extends JeecgController<CaClasss, ICaClasssService> {
	@Autowired
	private ICaClasssService caClasssService;
	
	/**
	 * 分页列表查询
	 *
	 * @param caClasss
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "班级-分页列表查询")
	@ApiOperation(value="班级-分页列表查询", notes="班级-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CaClasss caClasss,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CaClasss> queryWrapper = QueryGenerator.initQueryWrapper(caClasss, req.getParameterMap());
		Page<CaClasss> page = new Page<CaClasss>(pageNo, pageSize);
		IPage<CaClasss> pageList = caClasssService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param caClasss
	 * @return
	 */
	@AutoLog(value = "班级-添加")
	@ApiOperation(value="班级-添加", notes="班级-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CaClasss caClasss) {
		caClasssService.save(caClasss);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param caClasss
	 * @return
	 */
	@AutoLog(value = "班级-编辑")
	@ApiOperation(value="班级-编辑", notes="班级-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CaClasss caClasss) {
		caClasssService.updateById(caClasss);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "班级-通过id删除")
	@ApiOperation(value="班级-通过id删除", notes="班级-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		caClasssService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "班级-批量删除")
	@ApiOperation(value="班级-批量删除", notes="班级-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.caClasssService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "班级-通过id查询")
	@ApiOperation(value="班级-通过id查询", notes="班级-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CaClasss caClasss = caClasssService.getById(id);
		if(caClasss==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(caClasss);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param caClasss
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CaClasss caClasss) {
        return super.exportXls(request, caClasss, CaClasss.class, "班级");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CaClasss.class);
    }

}
