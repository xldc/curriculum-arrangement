package org.jeecg.modules.edu.ca.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.ca.entity.CaGrade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 年级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface CaGradeMapper extends BaseMapper<CaGrade> {

}
