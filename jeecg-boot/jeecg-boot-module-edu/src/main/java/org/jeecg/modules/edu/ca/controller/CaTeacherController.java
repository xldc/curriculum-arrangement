package org.jeecg.modules.edu.ca.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.ca.entity.CaTeacher;
import org.jeecg.modules.edu.ca.service.ICaTeacherService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 教师
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Api(tags="教师")
@RestController
@RequestMapping("/ca/caTeacher")
@Slf4j
public class CaTeacherController extends JeecgController<CaTeacher, ICaTeacherService> {
	@Autowired
	private ICaTeacherService caTeacherService;
	
	/**
	 * 分页列表查询
	 *
	 * @param caTeacher
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "教师-分页列表查询")
	@ApiOperation(value="教师-分页列表查询", notes="教师-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CaTeacher caTeacher,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CaTeacher> queryWrapper = QueryGenerator.initQueryWrapper(caTeacher, req.getParameterMap());
		Page<CaTeacher> page = new Page<CaTeacher>(pageNo, pageSize);
		IPage<CaTeacher> pageList = caTeacherService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param caTeacher
	 * @return
	 */
	@AutoLog(value = "教师-添加")
	@ApiOperation(value="教师-添加", notes="教师-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CaTeacher caTeacher) {
		caTeacherService.save(caTeacher);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param caTeacher
	 * @return
	 */
	@AutoLog(value = "教师-编辑")
	@ApiOperation(value="教师-编辑", notes="教师-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CaTeacher caTeacher) {
		caTeacherService.updateById(caTeacher);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "教师-通过id删除")
	@ApiOperation(value="教师-通过id删除", notes="教师-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		caTeacherService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "教师-批量删除")
	@ApiOperation(value="教师-批量删除", notes="教师-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.caTeacherService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "教师-通过id查询")
	@ApiOperation(value="教师-通过id查询", notes="教师-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CaTeacher caTeacher = caTeacherService.getById(id);
		if(caTeacher==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(caTeacher);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param caTeacher
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CaTeacher caTeacher) {
        return super.exportXls(request, caTeacher, CaTeacher.class, "教师");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CaTeacher.class);
    }

}
