package org.jeecg.modules.edu.ca.gap.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Time {

    private int weekOrder;
    private int courseOrder;
}
