package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaClassroom;
import org.jeecg.modules.edu.ca.mapper.CaClassroomMapper;
import org.jeecg.modules.edu.ca.service.ICaClassroomService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 教室
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Service
public class CaClassroomServiceImpl extends ServiceImpl<CaClassroomMapper, CaClassroom> implements ICaClassroomService {

}
