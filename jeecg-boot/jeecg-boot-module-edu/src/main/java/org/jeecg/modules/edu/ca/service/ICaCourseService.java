package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 课程
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
public interface ICaCourseService extends IService<CaCourse> {

}
