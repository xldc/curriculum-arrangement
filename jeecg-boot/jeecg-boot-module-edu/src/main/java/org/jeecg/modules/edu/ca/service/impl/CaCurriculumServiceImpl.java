package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaCurriculum;
import org.jeecg.modules.edu.ca.mapper.CaCurriculumMapper;
import org.jeecg.modules.edu.ca.service.ICaCurriculumService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 课表
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Service
public class CaCurriculumServiceImpl extends ServiceImpl<CaCurriculumMapper, CaCurriculum> implements ICaCurriculumService {

}
