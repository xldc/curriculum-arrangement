package org.jeecg.modules.edu.ca.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 班级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Data
@TableName("ca_classs")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ca_classs对象", description="班级")
public class CaClasss implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**编码*/
	@Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    private java.lang.String code;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**年级id*/
	@Excel(name = "年级id", width = 15)
    @ApiModelProperty(value = "年级id")
    private java.lang.String grade;
	/**年级编码*/
	@Excel(name = "年级编码", width = 15)
    @ApiModelProperty(value = "年级编码")
    private java.lang.String gradeCode;
	/**年级*/
	@Excel(name = "年级", width = 15)
    @ApiModelProperty(value = "年级")
    private java.lang.String gradeName;
	/**专业id*/
	@Excel(name = "专业id", width = 15)
    @ApiModelProperty(value = "专业id")
    private java.lang.String professional;
	/**专业编码*/
	@Excel(name = "专业编码", width = 15)
    @ApiModelProperty(value = "专业编码")
    private java.lang.String professionalCode;
	/**专业*/
	@Excel(name = "专业", width = 15)
    @ApiModelProperty(value = "专业")
    private java.lang.String professionalName;
	/**现有人数*/
	@Excel(name = "现有人数", width = 15)
    @ApiModelProperty(value = "现有人数")
    private java.lang.Integer existingNum;
	/**班主任id*/
	@Excel(name = "班主任id", width = 15)
    @ApiModelProperty(value = "班主任id")
    private java.lang.String headTeacher;
	/**班主任编码*/
	@Excel(name = "班主任编码", width = 15)
    @ApiModelProperty(value = "班主任编码")
    private java.lang.String headTeacherCode;
	/**班主任*/
	@Excel(name = "班主任", width = 15)
    @ApiModelProperty(value = "班主任")
    private java.lang.String headTeacherName;
}
