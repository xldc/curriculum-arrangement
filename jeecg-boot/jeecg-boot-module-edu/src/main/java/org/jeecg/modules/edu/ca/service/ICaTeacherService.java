package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 教师
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface ICaTeacherService extends IService<CaTeacher> {

}
