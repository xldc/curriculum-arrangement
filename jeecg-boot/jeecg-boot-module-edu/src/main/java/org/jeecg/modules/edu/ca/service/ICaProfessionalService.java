package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaProfessional;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专业
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface ICaProfessionalService extends IService<CaProfessional> {

}
