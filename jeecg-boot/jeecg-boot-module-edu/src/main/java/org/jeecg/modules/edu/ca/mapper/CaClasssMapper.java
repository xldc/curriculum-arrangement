package org.jeecg.modules.edu.ca.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.ca.entity.CaClasss;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 班级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface CaClasssMapper extends BaseMapper<CaClasss> {

}
