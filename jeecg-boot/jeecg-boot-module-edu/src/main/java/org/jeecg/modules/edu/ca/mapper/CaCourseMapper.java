package org.jeecg.modules.edu.ca.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.ca.entity.CaCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 课程
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
public interface CaCourseMapper extends BaseMapper<CaCourse> {

}
