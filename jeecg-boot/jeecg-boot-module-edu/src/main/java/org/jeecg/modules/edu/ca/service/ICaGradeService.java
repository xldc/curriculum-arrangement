package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaGrade;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 年级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface ICaGradeService extends IService<CaGrade> {

}
