package org.jeecg.modules.edu.ca.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.ca.entity.CaTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 教师
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface CaTeacherMapper extends BaseMapper<CaTeacher> {

}
