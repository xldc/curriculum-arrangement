package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaStudent;
import org.jeecg.modules.edu.ca.mapper.CaStudentMapper;
import org.jeecg.modules.edu.ca.service.ICaStudentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 学生
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaStudentServiceImpl extends ServiceImpl<CaStudentMapper, CaStudent> implements ICaStudentService {

}
