package org.jeecg.modules.edu.ca.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClassroomTypeEnum {

    GENERAL("普通教室",1),
    COMPUTER("计算机机房",2),
    PHYSICAL("体育场",3),
    EXPERIMENT("实验室",4),
    ;

    private String name;
    private Integer code;


}
