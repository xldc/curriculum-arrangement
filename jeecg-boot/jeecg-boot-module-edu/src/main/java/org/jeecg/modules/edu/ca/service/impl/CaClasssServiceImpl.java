package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaClasss;
import org.jeecg.modules.edu.ca.mapper.CaClasssMapper;
import org.jeecg.modules.edu.ca.service.ICaClasssService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 班级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaClasssServiceImpl extends ServiceImpl<CaClasssMapper, CaClasss> implements ICaClasssService {

}
