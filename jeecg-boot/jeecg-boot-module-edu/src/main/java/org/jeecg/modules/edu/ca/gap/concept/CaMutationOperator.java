package org.jeecg.modules.edu.ca.gap.concept;

import org.jgap.Configuration;
import org.jgap.IUniversalRateCalculator;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.MutationOperator;

public class CaMutationOperator extends MutationOperator {
    public CaMutationOperator() throws InvalidConfigurationException {
        super();
    }

    public CaMutationOperator(Configuration a_conf) throws InvalidConfigurationException {
        super(a_conf);
    }

    public CaMutationOperator(Configuration a_config, IUniversalRateCalculator a_mutationRateCalculator) throws InvalidConfigurationException {
        super(a_config, a_mutationRateCalculator);
    }

    public CaMutationOperator(Configuration a_config, int a_desiredMutationRate) throws InvalidConfigurationException {
        super(a_config, a_desiredMutationRate);
    }
}
