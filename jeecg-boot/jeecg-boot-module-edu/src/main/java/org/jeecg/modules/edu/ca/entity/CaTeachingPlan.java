package org.jeecg.modules.edu.ca.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Data
@TableName("ca_teaching_plan")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ca_teaching_plan对象", description="教学计划")
public class CaTeachingPlan implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**编码*/
	@Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    private java.lang.String code;
	/**学期id*/
	@Excel(name = "学期id", width = 15)
    @ApiModelProperty(value = "学期id")
    private java.lang.String semester;
	/**学期*/
	@Excel(name = "学期", width = 15)
    @ApiModelProperty(value = "学期")
    private java.lang.String semesterName;
	/**年级id*/
	@Excel(name = "年级id", width = 15)
    @ApiModelProperty(value = "年级id")
    private java.lang.String grade;
	/**年级编码*/
	@Excel(name = "年级编码", width = 15)
    @ApiModelProperty(value = "年级编码")
    private java.lang.String gradeCode;
	/**年级*/
	@Excel(name = "年级", width = 15)
    @ApiModelProperty(value = "年级")
    private java.lang.String gradeName;
	/**教师id*/
	@Excel(name = "教师id", width = 15)
    @ApiModelProperty(value = "教师id")
    private java.lang.String teacher;
	/**教师编码*/
	@Excel(name = "教师编码", width = 15)
    @ApiModelProperty(value = "教师编码")
    private java.lang.String teacherCode;
	/**教师*/
	@Excel(name = "教师", width = 15)
    @ApiModelProperty(value = "教师")
    private java.lang.String teacherName;
	/**课程id*/
	@Excel(name = "课程id", width = 15)
    @ApiModelProperty(value = "课程id")
    private java.lang.String course;
	/**课程编码*/
	@Excel(name = "课程编码", width = 15)
    @ApiModelProperty(value = "课程编码")
    private java.lang.String courseCode;
	/**课程*/
	@Excel(name = "课程", width = 15)
    @ApiModelProperty(value = "课程")
    private java.lang.String courseName;
	/**班级id*/
	@Excel(name = "班级id", width = 15)
    @ApiModelProperty(value = "班级id")
    private java.lang.String classs;
	/**班级编码*/
	@Excel(name = "班级编码", width = 15)
    @ApiModelProperty(value = "班级编码")
    private java.lang.String classsCode;
	/**班级*/
	@Excel(name = "班级", width = 15)
    @ApiModelProperty(value = "班级")
    private java.lang.String classsName;
	/**共多少周*/
	@Excel(name = "共多少周", width = 15)
    @ApiModelProperty(value = "共多少周")
    private java.lang.Integer weeks;
	/**每周多少节课*/
	@Excel(name = "每周多少节课", width = 15)
    @ApiModelProperty(value = "每周多少节课")
    private java.lang.Integer weeklyCourseNum;
	/**共多少节课*/
	@Excel(name = "共多少节课", width = 15)
    @ApiModelProperty(value = "共多少节课")
    private java.lang.Integer totalCourseNum;
}
