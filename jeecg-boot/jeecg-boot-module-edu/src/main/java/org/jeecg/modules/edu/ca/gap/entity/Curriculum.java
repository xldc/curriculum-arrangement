package org.jeecg.modules.edu.ca.gap.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Curriculum implements Serializable {
    private java.lang.String classs;
    private java.lang.String course;
    private java.lang.String teacher;
    private java.lang.String classroom;
    private int weekOrder;
    private int courseOrder;
}
