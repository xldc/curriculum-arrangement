package org.jeecg.modules.edu.ca.gap.io;

import org.apache.commons.lang.StringUtils;
import org.jeecg.modules.edu.ca.entity.CaClassroom;
import org.jeecg.modules.edu.ca.entity.CaCurriculum;
import org.jeecg.modules.edu.ca.entity.CaTeachingPlan;
import org.jeecg.modules.edu.ca.gap.concept.CaSuperGene;
import org.jeecg.modules.edu.ca.gap.core.*;
import org.jeecg.modules.edu.ca.gap.entity.TeachingPlan;
import org.jeecg.modules.edu.ca.gap.entity.Time;
import org.jeecg.modules.edu.ca.gap.util.Constant;
import org.jeecg.modules.edu.ca.mapper.CaCurriculumMapper;
import org.jgap.Chromosome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OutputData {
    @Autowired
    private CaCurriculumMapper caCurriculumMapper;

    public void printToConsole(Chromosome bestChromosome){
        int geneSize = Constant.PLAN_LIST.size();
        CaSuperGene[] s = new CaSuperGene[geneSize];

        Map<String, String[][]> map = new HashMap<>();
        for(int i = 0;i<geneSize;i++){
            s[i] = (CaSuperGene)bestChromosome.getGene(i);
            CaGene c = (CaGene) s[i].geneAt(Constant.CLASSROOM);
            CaGene t = (CaGene) s[i].geneAt(Constant.TIME);

            TeachingPlan plan = Constant.PLAN_LIST.get(i);
            Time time = Constant.TIME_LIST.get(t.getNumber());
            CaClassroom classroom = Constant.CLASSROOM_LIST.get(c.getNumber());

            CaCurriculum curriculum = new CaCurriculum();
            curriculum.setSemester(plan.getSemester())
                    .setSemesterName(plan.getSemesterName())
                    .setGrade(plan.getGrade())
                    .setGradeCode(plan.getGradeCode())
                    .setGradeName(plan.getGradeName())
                    .setClasss(plan.getClasss())
                    .setClasssCode(plan.getClasssCode())
                    .setClasssName(plan.getClasssName())
                    .setTeacher(plan.getTeacher())
                    .setTeacherCode(plan.getTeacherCode())
                    .setTeacherName(plan.getTeacherName())
                    .setCourse(plan.getCourse())
                    .setCourseCode(plan.getCourseCode())
                    .setCourseName(plan.getCourseName())
                    .setClassroom(classroom.getId())
                    .setClassroomCode(classroom.getCode())
                    .setClassroomName(classroom.getName())
                    .setWeekOrder(time.getWeekOrder())
                    .setCourseOrder(time.getCourseOrder());

            //入库保存
            //caCurriculumMapper.insert(curriculum);

            String[][] arr = map.get(curriculum.getClasssName());
            if(arr == null){
                arr = new String[10][7];
                for(int ii = 0;ii<10;ii++){
                    for(int jj = 0;jj < 7;jj++){
                        arr[ii][jj] = StringUtils.repeat("■", 17);
                    }
                }
            }
            arr[time.getCourseOrder()-1][time.getWeekOrder()-1] = new StringBuilder()
                    .append(plan.getTeacherName())
                    .append("/")
                    .append(plan.getCourseName())
                    .append("/")
                    .append(classroom.getName())
                    .toString();
            map.put(curriculum.getClasssName(),arr);
        }
        System.out.println("------------------------------------------------------------------------");

        for (Map.Entry<String,String[][]> entry:map.entrySet()) {
            System.out.println("-----------------------------["+entry.getKey()+"]---------------------");
            String[][] arr = entry.getValue();
            for (int i = 0; i < arr.length; i++) {
                System.out.print(String.format("第%d节\t", i+1));
                for (String cc : arr[i]) {
                    System.out.print(cc + "\t");
                }
                System.out.println("");
            }
        }
    }
}
