package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaClasss;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 班级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface ICaClasssService extends IService<CaClasss> {

}
