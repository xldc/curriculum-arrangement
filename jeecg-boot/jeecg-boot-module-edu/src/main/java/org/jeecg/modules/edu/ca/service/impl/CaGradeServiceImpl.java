package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaGrade;
import org.jeecg.modules.edu.ca.mapper.CaGradeMapper;
import org.jeecg.modules.edu.ca.service.ICaGradeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 年级
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaGradeServiceImpl extends ServiceImpl<CaGradeMapper, CaGrade> implements ICaGradeService {

}
