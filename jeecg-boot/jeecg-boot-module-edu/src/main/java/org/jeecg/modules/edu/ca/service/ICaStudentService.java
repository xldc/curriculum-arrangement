package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaStudent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 学生
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface ICaStudentService extends IService<CaStudent> {

}
