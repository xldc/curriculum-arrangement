package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaWorkConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 作息设置
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface ICaWorkConfigService extends IService<CaWorkConfig> {

}
