package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaWorkConfig;
import org.jeecg.modules.edu.ca.mapper.CaWorkConfigMapper;
import org.jeecg.modules.edu.ca.service.ICaWorkConfigService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 作息设置
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaWorkConfigServiceImpl extends ServiceImpl<CaWorkConfigMapper, CaWorkConfig> implements ICaWorkConfigService {

}
