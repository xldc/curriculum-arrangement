package org.jeecg.modules.edu.ca.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 作息设置
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Data
@TableName("ca_work_config")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ca_work_config对象", description="作息设置")
public class CaWorkConfig implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**1上午 2下午 3晚上*/
	@Excel(name = "1上午 2下午 3晚上", width = 15, dicCode = "ca_time_quantum")
	@Dict(dicCode = "ca_time_quantum")
    @ApiModelProperty(value = "1上午 2下午 3晚上")
    private java.lang.Integer timeQuantum;
	/**第几节*/
	@Excel(name = "第几节", width = 15)
    @ApiModelProperty(value = "第几节")
    private java.lang.Integer courseOrder;
	/**周一*/
	@Excel(name = "周一", width = 15)
    @ApiModelProperty(value = "周一")
    private java.lang.Integer monday;
	/**周二*/
	@Excel(name = "周二", width = 15)
    @ApiModelProperty(value = "周二")
    private java.lang.Integer tuesday;
	/**周三*/
	@Excel(name = "周三", width = 15)
    @ApiModelProperty(value = "周三")
    private java.lang.Integer wednesday;
	/**周四*/
	@Excel(name = "周四", width = 15)
    @ApiModelProperty(value = "周四")
    private java.lang.Integer thursday;
	/**周五*/
	@Excel(name = "周五", width = 15)
    @ApiModelProperty(value = "周五")
    private java.lang.Integer friday;
	/**周六*/
	@Excel(name = "周六", width = 15)
    @ApiModelProperty(value = "周六")
    private java.lang.Integer saturday;
	/**周日*/
	@Excel(name = "周日", width = 15)
    @ApiModelProperty(value = "周日")
    private java.lang.Integer weekday;
}
