package org.jeecg.modules.edu.ca.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 教室
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Data
@TableName("ca_classroom")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ca_classroom对象", description="教室")
public class CaClassroom implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**编码*/
	@Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    private java.lang.String code;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**容纳人数*/
	@Excel(name = "容纳人数", width = 15)
    @ApiModelProperty(value = "容纳人数")
    private java.lang.Integer size;
	/**教室类型*/
	@Excel(name = "教室类型", width = 15, dicCode = "classroom_type")
	@Dict(dicCode = "classroom_type")
    @ApiModelProperty(value = "教室类型")
    private java.lang.Integer type;
}
