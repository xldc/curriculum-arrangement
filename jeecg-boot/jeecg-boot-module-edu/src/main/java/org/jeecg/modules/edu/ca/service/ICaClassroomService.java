package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaClassroom;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 教室
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
public interface ICaClassroomService extends IService<CaClassroom> {

}
