package org.jeecg.modules.edu.ca.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 学期
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Data
@TableName("ca_semester")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ca_semester对象", description="学期")
public class CaSemester implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**学期开始日期*/
	@Excel(name = "学期开始日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "学期开始日期")
    private java.util.Date startDate;
	/**学期截止日期*/
	@Excel(name = "学期截止日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "学期截止日期")
    private java.util.Date endDate;
	/**学期开始年份*/
	@Excel(name = "学期开始年份", width = 15)
    @ApiModelProperty(value = "学期开始年份")
    private java.lang.Integer startYear;
	/**学期截止年份*/
	@Excel(name = "学期截止年份", width = 15)
    @ApiModelProperty(value = "学期截止年份")
    private java.lang.Integer endYear;
	/**教学周开始日期*/
	@Excel(name = "教学周开始日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "教学周开始日期")
    private java.util.Date eduStartDate;
	/**教学周结束日期*/
	@Excel(name = "教学周结束日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "教学周结束日期")
    private java.util.Date eduEndDate;
	/**1:第一学期 2:第二学期*/
	@Excel(name = "1:第一学期 2:第二学期", width = 15, dicCode = "ca_semester")
	@Dict(dicCode = "ca_semester")
    @ApiModelProperty(value = "1:第一学期 2:第二学期")
    private java.lang.Integer semesterOrder;
	/**是否是当前学期*/
	@Excel(name = "是否是当前学期", width = 15)
    @ApiModelProperty(value = "是否是当前学期")
    private java.lang.Integer current;
}
