package org.jeecg.modules.edu.ca.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.edu.ca.entity.CaTeachingPlan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
public interface ICaTeachingPlanService extends IService<CaTeachingPlan> {

    Result<?> createCurriculum();
}
