package org.jeecg.modules.edu.ca.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 课程
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Data
@TableName("ca_course")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ca_course对象", description="课程")
public class CaCourse implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**编码*/
	@Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    private java.lang.String code;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**课时数-预留*/
	@Excel(name = "课时数-预留", width = 15)
    @ApiModelProperty(value = "课时数-预留")
    private java.lang.Integer classsHours;
	/**学分*/
	@Excel(name = "学分", width = 15)
    @ApiModelProperty(value = "学分")
    private java.lang.Integer credit;
	/**教室类型*/
	@Excel(name = "教室类型", width = 15, dicCode = "classroom_type")
	@Dict(dicCode = "classroom_type")
    @ApiModelProperty(value = "教室类型")
    private java.lang.Integer classroomType;
	/**排课最优时段*/
	@Excel(name = "排课最优时段", width = 15, dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
	@Dict(dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
    @ApiModelProperty(value = "排课最优时段")
    private java.lang.String firstTime;
	/**排课第二时段*/
	@Excel(name = "排课第二时段", width = 15, dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
	@Dict(dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
    @ApiModelProperty(value = "排课第二时段")
    private java.lang.String secondTime;
	/**排课第三时段*/
	@Excel(name = "排课第三时段", width = 15, dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
	@Dict(dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
    @ApiModelProperty(value = "排课第三时段")
    private java.lang.String thirdTime;
	/**排课第四时段*/
	@Excel(name = "排课第四时段", width = 15, dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
	@Dict(dictTable = "ca_work_config", dicText = "course_order", dicCode = "course_order")
    @ApiModelProperty(value = "排课第四时段")
    private java.lang.String fourthTime;
}
