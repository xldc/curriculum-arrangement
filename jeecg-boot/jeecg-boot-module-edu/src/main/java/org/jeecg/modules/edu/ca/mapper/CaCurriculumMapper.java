package org.jeecg.modules.edu.ca.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.ca.entity.CaCurriculum;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 课表
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
public interface CaCurriculumMapper extends BaseMapper<CaCurriculum> {

}
