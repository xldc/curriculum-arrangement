package org.jeecg.modules.edu.ca.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.ca.entity.CaStudent;
import org.jeecg.modules.edu.ca.service.ICaStudentService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 学生
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Api(tags="学生")
@RestController
@RequestMapping("/ca/caStudent")
@Slf4j
public class CaStudentController extends JeecgController<CaStudent, ICaStudentService> {
	@Autowired
	private ICaStudentService caStudentService;
	
	/**
	 * 分页列表查询
	 *
	 * @param caStudent
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "学生-分页列表查询")
	@ApiOperation(value="学生-分页列表查询", notes="学生-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CaStudent caStudent,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CaStudent> queryWrapper = QueryGenerator.initQueryWrapper(caStudent, req.getParameterMap());
		Page<CaStudent> page = new Page<CaStudent>(pageNo, pageSize);
		IPage<CaStudent> pageList = caStudentService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param caStudent
	 * @return
	 */
	@AutoLog(value = "学生-添加")
	@ApiOperation(value="学生-添加", notes="学生-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CaStudent caStudent) {
		caStudentService.save(caStudent);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param caStudent
	 * @return
	 */
	@AutoLog(value = "学生-编辑")
	@ApiOperation(value="学生-编辑", notes="学生-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CaStudent caStudent) {
		caStudentService.updateById(caStudent);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "学生-通过id删除")
	@ApiOperation(value="学生-通过id删除", notes="学生-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		caStudentService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "学生-批量删除")
	@ApiOperation(value="学生-批量删除", notes="学生-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.caStudentService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "学生-通过id查询")
	@ApiOperation(value="学生-通过id查询", notes="学生-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CaStudent caStudent = caStudentService.getById(id);
		if(caStudent==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(caStudent);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param caStudent
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CaStudent caStudent) {
        return super.exportXls(request, caStudent, CaStudent.class, "学生");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CaStudent.class);
    }

}
