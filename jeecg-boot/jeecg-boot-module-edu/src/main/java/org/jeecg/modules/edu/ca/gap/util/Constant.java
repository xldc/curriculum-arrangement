package org.jeecg.modules.edu.ca.gap.util;

import org.jeecg.modules.edu.ca.entity.CaClassroom;
import org.jeecg.modules.edu.ca.entity.CaTeachingPlan;
import org.jeecg.modules.edu.ca.gap.entity.*;

import java.util.ArrayList;
import java.util.List;

public class Constant {

    public final static String TOKEN_SEPARATOR = ":";
    public final static String GENE_DELIMITER = "+";

    public static List<TeachingPlan> PLAN_LIST = new ArrayList<>();
    public static List<CaClassroom> CLASSROOM_LIST = new ArrayList<>();
    public static List<Time> TIME_LIST = new ArrayList<>();
    public static Integer CLASSROOM = 0;
    public static Integer TIME  = 1;
    /**
     * 每天最大课时数
     */
    public static Integer COURSE_NUM_BY_DAY  = 10;
    /**
     * 最大进化次数
     */
    public final static Integer MAX_EVOLUTIONS  = 1000;
    /**
     * 种群大小
     */
    public final static Integer POPULATION_SIZE = 50;
    /**
     * 达到该期望值结束进化 该值要大于致命冲突的结果
     */
    public final static Double THRESHOLD = 0.025;//1D;
}
