package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.edu.ca.entity.CaTeachingPlan;
import org.jeecg.modules.edu.ca.gap.GapCurriculumService;
import org.jeecg.modules.edu.ca.mapper.CaTeachingPlanMapper;
import org.jeecg.modules.edu.ca.service.ICaTeachingPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Service
public class CaTeachingPlanServiceImpl extends ServiceImpl<CaTeachingPlanMapper, CaTeachingPlan> implements ICaTeachingPlanService {

    @Autowired
    private GapCurriculumService gapCurriculumService;


    @Override
    public Result<?> createCurriculum() {
        if(gapCurriculumService.createCurriculum()){
            return Result.OK("排课成功");
        }
        return Result.error("排课失败");
    }
}
