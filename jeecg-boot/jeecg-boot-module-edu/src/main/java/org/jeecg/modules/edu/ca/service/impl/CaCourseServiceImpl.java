package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaCourse;
import org.jeecg.modules.edu.ca.mapper.CaCourseMapper;
import org.jeecg.modules.edu.ca.service.ICaCourseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 课程
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
@Service
public class CaCourseServiceImpl extends ServiceImpl<CaCourseMapper, CaCourse> implements ICaCourseService {

}
