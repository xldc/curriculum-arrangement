package org.jeecg.modules.edu.ca.service.impl;

import org.jeecg.modules.edu.ca.entity.CaTeacher;
import org.jeecg.modules.edu.ca.mapper.CaTeacherMapper;
import org.jeecg.modules.edu.ca.service.ICaTeacherService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 教师
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Service
public class CaTeacherServiceImpl extends ServiceImpl<CaTeacherMapper, CaTeacher> implements ICaTeacherService {

}
