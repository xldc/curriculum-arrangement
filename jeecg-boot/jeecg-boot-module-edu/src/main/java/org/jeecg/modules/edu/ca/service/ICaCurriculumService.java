package org.jeecg.modules.edu.ca.service;

import org.jeecg.modules.edu.ca.entity.CaCurriculum;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 课表
 * @Author: jeecg-boot
 * @Date:   2021-07-06
 * @Version: V1.0
 */
public interface ICaCurriculumService extends IService<CaCurriculum> {

}
