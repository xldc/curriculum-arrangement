package org.jeecg.modules.edu.ca.gap.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.jeecg.modules.edu.ca.entity.CaTeachingPlan;

@Data
@Accessors(chain = true)
public class TeachingPlan extends CaTeachingPlan {

    private Integer classsSize;
    private Integer classroomType;
    /**排课最优时段*/
    private java.lang.String firstTime;
    /**排课第二时段*/
    private java.lang.String secondTime;
    /**排课第三时段*/
    private java.lang.String thirdTime;
    /**排课第四时段*/
    private java.lang.String fourthTime;
}
